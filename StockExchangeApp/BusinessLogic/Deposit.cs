﻿using System;
using System.Collections.Generic;
using System.Linq;
using StockExchangeApp.Models;

namespace StockExchangeApp.BusinessLogic
{
    public class Deposit
    {
        private const int NumberOfDaysInYear = 365;
        private const int NumberOfMonthsInYear = 12;
        private const int NumberOfYearsInYear = 1;
        private const int MonthPeriodLengthInDays = 30;
        
        private readonly decimal amount;
        private readonly DateTime begin;
        private readonly decimal percentPerPeriod;
        private readonly double depositLengthInDays;
        private readonly int capitalizationPeriodLengthInDays;
        private readonly List<string> info;
        private decimal profit;


        public Deposit(int capitalizationsPerYear, decimal amount, decimal interestRate, DateTime begin, DateTime end)
        {
            this.amount = amount;
            this.begin = begin;
            percentPerPeriod = interestRate / capitalizationsPerYear;
            depositLengthInDays = (end - begin).TotalDays;
            capitalizationPeriodLengthInDays = GetCapitalizationPeriodLengthInDays(capitalizationsPerYear);
            info = new List<string>();
            if (IsDateRangeShorterThanCapitalizationPeriod())
            {
                info.Add("Wybrany okres jest krótszy niż okres kapitalizacji - lokata nie przyniesie żadnych zysków");
            }
        }

        public IList<DataRecordViewModel> GetDepositData()
        {
            var records = new List<DataRecordViewModel>();
            
            var value = amount;
            var daysCounter = 0;

            records.Add(new DataRecordViewModel()
            {
                Date = begin.ToShortDateString(),
                Value = value.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture)
            });

            for (var i = 1; i <= depositLengthInDays; i++)
            {
                daysCounter++;
                if (IsCapitalizationDay(daysCounter))
                {
                    var interests = value * percentPerPeriod / 100;
                    value += interests;
                    daysCounter = 0;
                }

                records.Add(new DataRecordViewModel()
                {
                    Date = begin.AddDays(i).ToShortDateString(),
                    Value = value.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture)
                });
            }
            profit = value - amount;
            return records;
        }

        private bool IsCapitalizationDay(int daysCounter)
        {
            return daysCounter == capitalizationPeriodLengthInDays;
        }

        private int GetCapitalizationPeriodLengthInDays(int capitalizationsPerYear)
        {
            var length = 0;
            if (capitalizationsPerYear == NumberOfYearsInYear)
            {
                length = NumberOfDaysInYear;
            }
            else if (capitalizationsPerYear == NumberOfMonthsInYear)
            {
                length = MonthPeriodLengthInDays;
            }
            else
            {
                length = NumberOfYearsInYear;
            }
            return length;
        }

        public bool IsDateRangeShorterThanCapitalizationPeriod()
        {
            return depositLengthInDays < capitalizationPeriodLengthInDays;
        }

        public static List<Tuple<string, int>> GetCapitalizations()
        {
            var capitalizations = new List<Tuple<string, int>>
            {
                new Tuple<string, int>("1 dzień", NumberOfDaysInYear),
                new Tuple<string, int>("1 miesiąc", NumberOfMonthsInYear),
                new Tuple<string, int>("1 rok", NumberOfYearsInYear)
            };
            return capitalizations;
        }

        public decimal GetProfit()
        {
            return Math.Round(profit, 2);
        }
        public List<string> GetInfo()
        {
            return info;
        }
    }
}