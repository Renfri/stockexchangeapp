﻿using StockExchangeApp.DatabaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using StockExchangeApp.Models;

namespace StockExchangeApp.BusinessLogic
{
    public class Fund
    {
        private readonly StockExchangeDBContext db = new StockExchangeDBContext();

        private readonly DateTime begin;
        private readonly DateTime end;
        private readonly decimal amount;
        private readonly List<StockRecord> dbRecords;
        private readonly double periodLengthInDays;
        private readonly decimal numberOfUnits;
        private readonly StockRecord lastRecord;

        private List<string> info;

        public Fund(decimal amount, DateTime begin, DateTime end)
        {
            info = new List<string>();
            this.amount = amount;
            this.begin = begin;
            this.end = end;
            dbRecords =
                db.StockRecords.ToList()
                    .Where(r => r.Data >= begin && r.Data <= end)
                    .OrderBy(r => r.Data)
                    .ToList();
            var startingRecord = GetStartingRecord();
            periodLengthInDays = (end - begin).TotalDays;
            numberOfUnits = GetNumberOfUnits(startingRecord);
            lastRecord = FindLastRecord();
        }

        private decimal GetNumberOfUnits(StockRecord record)
        {
            return record == null ? 0 : Math.Floor(amount / record.Wartosc.Value * 100)/100;
        }

        public IList<DataRecordViewModel> GetFundData()
        {
            var records = new List<DataRecordViewModel>();

            var currentDate = begin;
            for (var i = 0; i <= periodLengthInDays; i++)
            {
                var value = dbRecords.FirstOrDefault(r => r.Data == currentDate)?.Wartosc;
                
                if (value != null)
                {
                    value *= numberOfUnits;
                }
                records.Add(new DataRecordViewModel()
                {
                    Date = currentDate.ToShortDateString(),
                    Value = value?.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture) ?? "null"
                });
                currentDate = currentDate.AddDays(1);
            }

            return records;
        }

        private StockRecord GetStartingRecord()
        {
            if (info == null)
            {
                info = new List<string>();
            }

            if (dbRecords == null || !dbRecords.Any() || dbRecords.All(r => r.Wartosc == null))
            {
                info.Add("Brak danych dotyczących kursu giełdowego z wybranego okresu.");
                return null;
            }

            var startingRecord = dbRecords[0];

            if (startingRecord.Wartosc != null)
            {
                return startingRecord;
            }

            startingRecord = dbRecords.FirstOrDefault(r => r.Wartosc != null);
            info.Add(
                "Brak danych dotyczących kursu giełdowego z wybranego dnia początkowego. Wyliczenia zysku z funduszu zostały przedstawione dla następującego dnia rozpoczęcia: " +
                startingRecord.Data.ToShortDateString()
                );

            return startingRecord;
        }

        public decimal GetProfit()
        {
            return lastRecord == null ? 0 : Math.Round(lastRecord.Wartosc.Value*numberOfUnits - amount, 2);
        }

        private StockRecord FindLastRecord()
        {
            if (info == null)
            {
                info = new List<string>();
            }
            var records = dbRecords.OrderByDescending(r => r.Data);
            var last = records.FirstOrDefault(record => record.Wartosc.HasValue);
            if (last != null && last.Data != end)
            {
                info.Add(
                "Brak danych dotyczących kursu giełdowego z wybranego dnia końcowego. Wyliczenia zysku z funduszu zostały przedstawione do następującego dnia: " +
                last.Data.ToShortDateString()
                );
            }
            return last;
        }

        public List<string> GetInfo()
        {
            return info;
        } 
    }
}