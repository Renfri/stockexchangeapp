﻿using StockExchangeApp.DatabaseClasses;
using StockExchangeApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PagedList;

namespace StockExchangeApp.Controllers
{
    public class HomeController : Controller
    {
        private StockExchangeDBContext db = new StockExchangeDBContext();

        public ActionResult Index(DateTime? beginDate, DateTime? endDate, int? page)
        {
            ViewBag.Title = "Home";

            var pageSize = 50;
            var pageNumber = (page ?? 1);

            MainPageViewModel model;

            if (beginDate.HasValue && endDate.HasValue)
            {
                model = CreateFilteredViewModel(beginDate.Value, endDate.Value);
            }
            else
            {
                model = CreateUnfilteredViewModel();

            }

            model.PagedStockRecords = model.StockRecords.ToPagedList(pageNumber, pageSize);
            return View(model);
        }

        private MainPageViewModel CreateFilteredViewModel(DateTime beginDate, DateTime endDate)
        {
            var records =
                db.StockRecords.ToList<StockRecord>()
                    .Where(r => r.Data >= beginDate && r.Data <= endDate)
                    .OrderBy(r => r.Data)
                    .ToList();

            var viewModelRecords = ChangeTypeToDataRecordViewModel(records);

            var model = new MainPageViewModel()
            {
                BeginDate = beginDate,
                EndDate = endDate,
                StockRecords = viewModelRecords
            };
            model.BeginDate = beginDate;
            model.EndDate = endDate;
            return model;
        }

        private MainPageViewModel CreateUnfilteredViewModel()
        {
            var model = new MainPageViewModel();

            var records = db.StockRecords.OrderBy(r => r.Data).ToList();

            var viewModelRecords = ChangeTypeToDataRecordViewModel(records);

            model.StockRecords = viewModelRecords;

            model.BeginDate = records.FirstOrDefault().Data;
            model.EndDate = records.LastOrDefault().Data;
            return model;
        }

        private static IList<DataRecordViewModel> ChangeTypeToDataRecordViewModel(IList<StockRecord> records)
        {
            return records.Select(r => new DataRecordViewModel()
            {
                Date = r.Data.ToShortDateString(),
                Value = r.Wartosc?.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture) ?? "null"
            }).ToList();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
