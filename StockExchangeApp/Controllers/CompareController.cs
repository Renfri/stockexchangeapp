﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using StockExchangeApp.BusinessLogic;
using StockExchangeApp.DatabaseClasses;
using StockExchangeApp.Models;

namespace StockExchangeApp.Controllers
{
    public class CompareController : Controller
    {
        private StockExchangeDBContext db = new StockExchangeDBContext();

        public ActionResult Index()
        {
            var model = new ComparePageViewModel();
            InitializeModelFields(model);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "Amount,Interests,BeginDate,EndDate,SelectedCapitalization")] ComparePageViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            InitializeModelFields(model);

            var fund = new Fund(model.Amount, model.BeginDate, model.EndDate);

            model.StockRecords = fund.GetFundData();
            model.StockProfit = fund.GetProfit();
            var info = fund.GetInfo();
            foreach (var item in info)
            {
                model.Info.Add(item);
            }

            var deposit = new Deposit(int.Parse(model.SelectedCapitalization), model.Amount, model.Interests, model.BeginDate, model.EndDate);
            model.DepositRecords = deposit.GetDepositData();
            model.DepositProfit = deposit.GetProfit();
            info = deposit.GetInfo();
            foreach (var item in info)
            {
                model.Info.Add(item);
            }

            return View("Index", model);
        }

        private void InitializeModelFields(ComparePageViewModel model)
        {
            model.Capitalization = CreateCapitalizationList();
            model.Info = new List<string>();
            model.DepositRecords = new List<DataRecordViewModel>();
            model.StockRecords = new List<DataRecordViewModel>();
        }

        private List<SelectListItem> CreateCapitalizationList()
        {
            var capitalizations = Deposit.GetCapitalizations();

            var listItems = capitalizations.Select(capitalization => new SelectListItem()
            {
                Text = capitalization.Item1, Value = capitalization.Item2.ToString()
            }).ToList();

            listItems[0].Selected = true;
            return listItems;
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}