﻿function validateForm() {
    var validationInfoDiv = $("#validationInfo");
    validationInfoDiv.empty();
    var serverSideValidationInfo = $("#serverSideValidationInfo");
    if (!(serverSideValidationInfo == null) && serverSideValidationInfo.children().length > 0) {
        serverSideValidationInfo.hide();
    }
    validateDates(validationInfoDiv);
    validateValues(validationInfoDiv);
    if (validationInfoDiv.children().length === 0) {
        validationInfoDiv.hide();
        return true;
    } else {
        validationInfoDiv.show();
        return false;
    }
}

function validateDates(validationInfoDiv) {
    var begin = new Date(document.forms["form"]["beginDate"].value);
    var end = new Date(document.forms["form"]["endDate"].value);

    if (isNaN(begin.getTime())) {
        validationInfoDiv.append("<p class=\"alert-danger\">Wprowadź poprawną datę początkową.</p>");
    }
    if (isNaN(end.getTime())) {
        validationInfoDiv.append("<p class=\"alert-danger\">Wprowadź poprawną datę końcową.</p>");
    }
    if (begin > end) {
        validationInfoDiv.append("<p class=\"alert-danger\">Data końcowa musi być późniejsza niż początkowa.</p>");
    }
}

function validateValues(validationInfoDiv) {
    var amount = $("#Amount").val();
    var interests = $("#Interests").val();
    if (amount == null || interests == null) {
        return;
    }
    amount = amount.replace(',','.');
    interests = interests.replace(',', '.');
    if (isNaN(amount)) {
        validationInfoDiv.append("<p class=\"alert-danger\">Kwota musi być liczbą.</p>");       
    }
    if (isNaN(interests)) {
        validationInfoDiv.append("<p class=\"alert-danger\">Oprocentowanie musi być liczbą.</p>");
    }
    if (amount < 0) {
        validationInfoDiv.append("<p class=\"alert-danger\">Wprowadź nieujemną kwotę.</p>");     
    }
    if (interests < 0) {
        validationInfoDiv.append("<p class=\"alert-danger\">Wprowadź nieujemne oprocentowanie.</p>");
    }
}