﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace StockExchangeApp.Models
{
    public class ComparePageViewModel
    {
        [Display(Name = "Kwota [zł]")]
        [Required(ErrorMessage = "Pole wymagane")]
        public decimal Amount { get; set; }

        [Display(Name = "Oprocentowanie roczne [%]")]
        [Required(ErrorMessage = "Pole wymagane")]
        public decimal Interests { get; set; }

        [Display(Name = "Data rozpoczęcia")]
        [Required(ErrorMessage = "Pole wymagane")]
        [DataType(DataType.Date)]
        public DateTime BeginDate { get; set; }

        [Display(Name = "Data zakończenia")]
        [Required(ErrorMessage = "Pole wymagane")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        [Display(Name = "Zysk z lokaty")]
        public decimal DepositProfit { get; set; }

        [Display(Name = "Zysk z funduszu")]
        public decimal StockProfit { get; set; }

        public IEnumerable<DataRecordViewModel> StockRecords { get; set; }

        public IEnumerable<DataRecordViewModel> DepositRecords { get; set; }

        public IList<string> Info { get; set; }

        [Display(Name = "Okres kapitalizacji")]
        public IList<SelectListItem> Capitalization { get; set; }

        public string SelectedCapitalization { get; set; }
    }
}