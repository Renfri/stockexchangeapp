﻿using System.ComponentModel.DataAnnotations;

namespace StockExchangeApp.Models
{
    public class DataRecordViewModel
    {
        [Display(Name = "Data")]
        public string Date { get; set; }

        [Display(Name = "Wartość")]
        public string Value { get; set; }
    }
}