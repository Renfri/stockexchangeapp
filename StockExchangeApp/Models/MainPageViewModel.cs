﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PagedList;

namespace StockExchangeApp.Models
{
    public class MainPageViewModel
    {
        [Display(Name = "Początek")]
        [DataType(DataType.Date, ErrorMessage = "Wprowadź poprawną datę")]
        [Required(ErrorMessage = "Pole wymagane")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime BeginDate { get; set; }

        [Display(Name = "Koniec")]
        [DataType(DataType.Date, ErrorMessage = "Wprowadź poprawną datę")]
        [Required(ErrorMessage = "Pole wymagane")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }

        public IEnumerable<DataRecordViewModel> StockRecords { get; set; }

        public IPagedList<DataRecordViewModel> PagedStockRecords { get; set; }
    }
        
}