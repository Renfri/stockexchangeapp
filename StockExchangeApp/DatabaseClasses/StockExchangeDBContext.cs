﻿using System.Data.Entity;

namespace StockExchangeApp.DatabaseClasses
{
    public class StockExchangeDBContext : DbContext
    {
        public StockExchangeDBContext() : base("name=StockExchangeDBConnectionString")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<StockExchangeDBContext, Configuration>());
        }

        public IDbSet<StockRecord> StockRecords { get; set; }
    }  
}