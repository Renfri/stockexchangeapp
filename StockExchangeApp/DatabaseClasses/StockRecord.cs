﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StockExchangeApp.DatabaseClasses
{
    public class StockRecord
    {
        public int Id { get; set; }

        [Display(Name = "Data")]
        public DateTime Data { get; set; }

        [Display(Name = "Wartość")]
        public decimal? Wartosc { get; set; }
    }
}