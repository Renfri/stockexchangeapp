﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using CsvReader = StockExchangeApp.DatabaseClasses.Csv.CsvReader;

namespace StockExchangeApp.DatabaseClasses
{
    public class StockExchangeDBInitializer : CreateDatabaseIfNotExists<StockExchangeDBContext>
    {
        protected override void Seed(StockExchangeDBContext context)
        {
            var reader = new CsvReader("StockExchangeApp.DatabaseClasses.Csv.data.csv");
            var records = reader.Read();
            foreach (var rec in records)
            {
                context.StockRecords.AddOrUpdate(rec);
            }
            context.SaveChanges();
        }
    }
}