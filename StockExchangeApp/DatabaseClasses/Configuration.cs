﻿using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using EntityFramework.BulkInsert.Extensions;
using CsvReader = StockExchangeApp.DatabaseClasses.Csv.CsvReader;

namespace StockExchangeApp.DatabaseClasses
{
    public class Configuration : DbMigrationsConfiguration<StockExchangeDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(StockExchangeDBContext context)
        {
            if (context.StockRecords.Any())
            {
                return;
            }
            var reader = new CsvReader("StockExchangeApp.DatabaseClasses.Csv.data.csv");
            var records = reader.Read();
            using (var tx = new TransactionScope())
            {
                context.BulkInsert(records, SqlBulkCopyOptions.KeepNulls);
                tx.Complete();
            }
        }
    }
}