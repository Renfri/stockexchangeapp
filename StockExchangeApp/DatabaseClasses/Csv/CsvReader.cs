using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using MoreLinq;

namespace StockExchangeApp.DatabaseClasses.Csv
{
    public class CsvReader
    {
        private readonly string resourceName;

        public CsvReader(string resourceName)
        {
            this.resourceName = resourceName;
        }

        public IList<StockRecord> Read()
        {
            var assembly = Assembly.GetExecutingAssembly();
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    var csvReader = new CsvHelper.CsvReader(reader);
                    csvReader.Configuration.RegisterClassMap<CustomClassMap>();
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    var records = csvReader.GetRecords<StockRecord>().ToArray();
                    var filledRecords = FillMissingRecords(records);
                    return filledRecords;
                }
            }
        }

        private IList<StockRecord> FillMissingRecords(StockRecord[] records)
        {
            var start = records.MinBy(r => r.Data).Data;
            var end = records.MaxBy(r => r.Data).Data;
            var range = Enumerable.Range(0, (int)(end - start).TotalDays + 1).Select(i => start.AddDays(i));
            var existingDates = records.Select(r => r.Data);
            var missing = range.Except(existingDates);

            var missingRecords = missing.Select(d => new StockRecord()
            {
                Data = d,
                Wartosc = null
            });
            var filledRecords = new List<StockRecord>();
            filledRecords.AddRange(records);
            filledRecords.AddRange(missingRecords);
            filledRecords.Sort(delegate (StockRecord x, StockRecord y)
            {
                if (x.Data == y.Data) return 0;
                else if (x.Data < y.Data) return -1;
                else return 1;
            });
            return filledRecords;
        }
    }
}