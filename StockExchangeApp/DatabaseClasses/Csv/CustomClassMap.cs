﻿using CsvHelper.Configuration;

namespace StockExchangeApp.DatabaseClasses.Csv
{
    public sealed class CustomClassMap : CsvClassMap<StockRecord>
    {
        public CustomClassMap()
        {
            Map(m => m.Data).Index(0).TypeConverter<DateConverter>();
            Map(m => m.Wartosc).Index(1).TypeConverter<DecimalConverter>();
        }
    }
}