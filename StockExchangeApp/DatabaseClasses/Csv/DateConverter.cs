﻿using System;
using CsvHelper.TypeConversion;

namespace StockExchangeApp.DatabaseClasses.Csv
{
    public class DateConverter : DefaultTypeConverter
    {
        public override object ConvertFromString(TypeConverterOptions options, string text)
        {
            try
            {
                var d = DateTime.ParseExact(text, "dd\"/\"MM\"/\"yyyy", System.Globalization.CultureInfo.InvariantCulture);
                return d;
            }
            catch
            {
                return base.ConvertFromString(options, text);
            }

        }
        public override bool CanConvertFrom(Type type)
        {
            return type == typeof(string);
        }
    }
}