﻿using System;
using CsvHelper.TypeConversion;

namespace StockExchangeApp.DatabaseClasses.Csv
{
    public class DecimalConverter : DefaultTypeConverter
    {
        public override object ConvertFromString(TypeConverterOptions options, string text)
        {
            try
            {
                var d = decimal.Parse(text, System.Globalization.CultureInfo.InvariantCulture);
                return d;
            }
            catch
            {
                return base.ConvertFromString(options, text);
            }

        }
        public override bool CanConvertFrom(Type type)
        {
            return type == typeof(string);
        }
    }
}